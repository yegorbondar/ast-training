package edu.ast.main

import edu.ast.util.Marker

@Marker
class Main {

    static void main(String[] args) {
        new Main().run()
    }

    def run() {
        println 'Running main'

        assert this.class.declaredMethods.find { it.name == 'added' }
        added()
    }

}
