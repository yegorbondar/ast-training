package edu.ast.main

import edu.ast.util.Execute

/**
 * Created by ybondar on 10.05.2014.
 */
class Executable {

    @Execute
    public void greet() {
        println "Hello from the greet() method!"
    }

}
