package edu.ast.util

import spock.lang.Specification

import java.lang.reflect.Field
import java.lang.reflect.Method

/**
 * Created by ybondar on 10.05.2014.
 */
class FieldTransformationSpec  extends Specification {

    // Testing transforms within the project requires evaluate
    def 'transform adds method'() {
        given:
        String classString = '''
class Test { }

new Test()
'''

        when:
        def instance = new GroovyShell().evaluate(classString)
        Field author = instance.class.declaredFields.find { it.name == 'author' }

        then:
        author.get(instance) == 'ybondar'
    }
}
