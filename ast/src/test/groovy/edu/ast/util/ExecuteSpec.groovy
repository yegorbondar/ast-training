package edu.ast.util

import spock.lang.Specification

import java.lang.reflect.Method

/**
 * Created by ybondar on 10.05.2014.
 */
class ExecuteSpec extends Specification{
    // Testing transforms within the project requires evaluate
    def 'transform adds main method'() {
        given:
        String classString = '''
import edu.ast.util.*


class Test { @Execute public void greet(){println "greet"} }

new Test().main(null)
'''

        when:
        def instance = new GroovyShell().evaluate(classString)
        Method added = instance.class.declaredMethods.find { it.name == 'main' }

        then:
        added
    }
}
